#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/timer.h>
#include <libopencm3/stm32/dma.h>
#include <libopencm3/cm3/nvic.h>

#include "firmware.h"
#include "hardware.h"
#include "config.h"

#define PLAYBACK_TIMER DEV_(TIM, playback_timer)
#define PLAYBACK_TIMER_RCC RCC_(TIM, playback_timer)
#define PLAYBACK_TIMER_OCX _CAT2(TIM_OC, playback_timer_ocx)
#define PLAYBACK_TIMER_OCXN _CAT3(TIM_OC, playback_timer_ocx, N)
#define PLAYBACK_TIMER_OCY _CAT2(TIM_OC, playback_timer_ocy)
#define PLAYBACK_TIMER_OCYN _CAT3(TIM_OC, playback_timer_ocy, N)
#define PLAYBACK_DMA DEV_(DMA, playback_dma)
#define PLAYBACK_DMA_CHANNEL DMA_CH_(playback_dma_ch)
#define PLAYBACK_DMA_IRQ DMA_IRQ_(playback_dma, playback_dma_ch)
#define PLAYBACK_DMA_ISR DMA_ISR_(playback_dma, playback_dma_ch)
#define PLAYBACK_PORT_P DEV_(GPIO, playback_port_p)
#define PLAYBACK_PADS_P OR2_(GPIO, playback_pads_p)
#define PLAYBACK_PORT_N DEV_(GPIO, playback_port_n)
#define PLAYBACK_PADS_N OR2_(GPIO, playback_pads_n)

void playback_convert(void) {
  /* unpack signed samples for playing using DMA */
  sample_t *r = &raw_buffer_data[raw_buffer_index].sample[SND_RAW_FRAME_SIZE - 1];
  playback_sample_t *w = &raw_buffer_data[raw_buffer_index].playback[SND_RAW_FRAME_SIZE - 1];

  for (; r >= raw_buffer_data[raw_buffer_index].sample; r--, w--) {
    if (*r >= 0) {
      w->positive = *r;
      w->negative = 0;
    } else {
      w->negative = -*r;
      w->positive = 0;
    }
  }
}

static void pwm_oc_disable(enum tim_oc_id oc_id, enum tim_oc_id ocn_id) {
  /* Disable output */
  timer_disable_oc_output(PLAYBACK_TIMER, oc_id);
  timer_disable_oc_output(PLAYBACK_TIMER, ocn_id);
}

static void pwm_oc_enable(enum tim_oc_id oc_id, enum tim_oc_id ocn_id) {
  /* Disable output */
  pwm_oc_disable(oc_id, ocn_id);
  
  /* Configure global mode of line */
  timer_disable_oc_clear(PLAYBACK_TIMER, oc_id);
  timer_enable_oc_preload(PLAYBACK_TIMER, oc_id);
  timer_set_oc_slow_mode(PLAYBACK_TIMER, oc_id);
  timer_set_oc_mode(PLAYBACK_TIMER, oc_id, TIM_OCM_PWM1);
  
  /* Configure OC */
  timer_set_oc_polarity_high(PLAYBACK_TIMER, oc_id);
  timer_set_oc_idle_state_set(PLAYBACK_TIMER, oc_id);

  timer_set_oc_polarity_high(PLAYBACK_TIMER, ocn_id);
  timer_set_oc_idle_state_set(PLAYBACK_TIMER, ocn_id);
  
  /* Set the output compare value for OC */
  timer_set_oc_value(PLAYBACK_TIMER, oc_id, 0);
  
  /* Enable output */
  timer_enable_oc_output(PLAYBACK_TIMER, oc_id);
  timer_enable_oc_output(PLAYBACK_TIMER, ocn_id);
}

void playback_resume(void) {
  /* Start DMA transfer */
  dma_enable_channel(PLAYBACK_DMA, PLAYBACK_DMA_CHANNEL);

  /* Enable counter */
  timer_enable_counter(PLAYBACK_TIMER);
}

void playback_suspend(void) {
  /* Disable counter */
  timer_disable_counter(PLAYBACK_TIMER);

  /* Stop DMA transfer */
  dma_disable_channel(PLAYBACK_DMA, PLAYBACK_DMA_CHANNEL);
}

void playback_sample(sample_t sample) {
  uint16_t hi = 0, lo = 0;
  if (sample > 0) {
    hi = sample;
  } else if (sample < 0) {
    lo = -sample;
  }
  timer_set_oc_value(PLAYBACK_TIMER, PLAYBACK_TIMER_OCX, hi);
  timer_set_oc_value(PLAYBACK_TIMER, PLAYBACK_TIMER_OCY, lo);
}

void playback_init(void) {
  /* Enable timer clock */
  rcc_periph_clock_enable(PLAYBACK_TIMER_RCC);

  /* Enable DMA interrupt */
  nvic_enable_irq(PLAYBACK_DMA_IRQ);

  /* Configure GPIO for PWM output */
  gpio_set_mode(PLAYBACK_PORT_P, GPIO_MODE_OUTPUT_50_MHZ,
                GPIO_CNF_OUTPUT_ALTFN_PUSHPULL,
                PLAYBACK_PADS_P);

  gpio_set_mode(PLAYBACK_PORT_N, GPIO_MODE_OUTPUT_50_MHZ,
                GPIO_CNF_OUTPUT_ALTFN_PUSHPULL,
                PLAYBACK_PADS_N);

  /* Reset TIM peripheral */
  timer_reset(PLAYBACK_TIMER);

  /* Timer global mode:
   * - No divider
   * - Alignment edge
   * - Direction up
   */
  timer_set_mode(PLAYBACK_TIMER, TIM_CR1_CKD_CK_INT_MUL_4,
                 TIM_CR1_CMS_EDGE, TIM_CR1_DIR_UP);

  /* Reset prescaler value */
  timer_set_prescaler(PLAYBACK_TIMER, 0);

  /* Reset repetition counter value */
	timer_set_repetition_counter(PLAYBACK_TIMER, 0);

  /* Enable preload */
  timer_enable_preload(PLAYBACK_TIMER);

  /* Continuous mode */
  timer_continuous_mode(PLAYBACK_TIMER);

  /* Period is a half of sound sample width */
  /* For example, 8-bit sample is 128 ticks */
  /* 562 KHz */
  timer_set_period(PLAYBACK_TIMER, SND_SAMPLE_WIDTH / 2);

  /* Configure break and deadtime */
  timer_set_deadtime(PLAYBACK_TIMER, 0);
	timer_set_enabled_off_state_in_idle_mode(PLAYBACK_TIMER);
	timer_set_enabled_off_state_in_run_mode(PLAYBACK_TIMER);
	timer_disable_break(PLAYBACK_TIMER);
	timer_set_break_polarity_high(PLAYBACK_TIMER);
	timer_disable_break_automatic_output(PLAYBACK_TIMER);
	timer_set_break_lock(PLAYBACK_TIMER, TIM_BDTR_LOCK_OFF);

  /* Enable outputs */
  pwm_oc_enable(PLAYBACK_TIMER_OCX, PLAYBACK_TIMER_OCXN);
  pwm_oc_enable(PLAYBACK_TIMER_OCY, PLAYBACK_TIMER_OCYN);

  /* ARR reload enable. */
  timer_enable_preload(PLAYBACK_TIMER);

  /* Enable preload of complementary channel configurations and update on COM event */
  timer_enable_preload_complementry_enable_bits(PLAYBACK_TIMER);

  /* Enable outputs in the break subsystem. */
  timer_enable_break_main_output(PLAYBACK_TIMER);
  
  /* Configure burst transfer (CCR1, CCR2) */
  TIM_DCR(PLAYBACK_TIMER) = ((2 - 1) << 8) | (&TIM_CCR1(PLAYBACK_TIMER) - &TIM_CR1(PLAYBACK_TIMER));

  /* Configure DMA */
  dma_channel_reset(PLAYBACK_DMA, PLAYBACK_DMA_CHANNEL);
  dma_set_priority(PLAYBACK_DMA, PLAYBACK_DMA_CHANNEL, DMA_CCR_PL_VERY_HIGH);
  dma_set_peripheral_size(PLAYBACK_DMA, PLAYBACK_DMA_CHANNEL, DMA_CCR_PSIZE_16BIT);
  dma_set_memory_size(PLAYBACK_DMA, PLAYBACK_DMA_CHANNEL, DMA_CCR_MSIZE_8BIT);
  dma_enable_memory_increment_mode(PLAYBACK_DMA, PLAYBACK_DMA_CHANNEL);
  dma_enable_circular_mode(PLAYBACK_DMA, PLAYBACK_DMA_CHANNEL);
  dma_set_read_from_memory(PLAYBACK_DMA, PLAYBACK_DMA_CHANNEL);
  dma_set_peripheral_address(PLAYBACK_DMA, PLAYBACK_DMA_CHANNEL, (uint32_t)&TIM_DMAR(PLAYBACK_TIMER));
  dma_set_memory_address(PLAYBACK_DMA, PLAYBACK_DMA_CHANNEL, (uint32_t)raw_buffer_data);
  dma_set_number_of_data(PLAYBACK_DMA, PLAYBACK_DMA_CHANNEL, sizeof(raw_buffer_data));

  /* Enable DMA interrupts */
  dma_enable_half_transfer_interrupt(PLAYBACK_DMA, PLAYBACK_DMA_CHANNEL);
  dma_enable_transfer_complete_interrupt(PLAYBACK_DMA, PLAYBACK_DMA_CHANNEL);
}

void playback_done(void) {
  /* Disable outputs */
  pwm_oc_disable(PLAYBACK_TIMER_OCX, PLAYBACK_TIMER_OCXN);
  pwm_oc_disable(PLAYBACK_TIMER_OCY, PLAYBACK_TIMER_OCYN);

  /* Counter disable */
  timer_disable_counter(PLAYBACK_TIMER);

  /* Disable DMA interrupt */
  nvic_disable_irq(PLAYBACK_DMA_IRQ);

  /* Disable timer clock */
  rcc_periph_clock_disable(PLAYBACK_TIMER_RCC);
}

void PLAYBACK_DMA_ISR(void) {
  dma_clear_interrupt_flags(PLAYBACK_DMA, PLAYBACK_DMA_CHANNEL, DMA_GIF | DMA_TCIF | DMA_HTIF);

  gpio_toggle(GPIOA, GPIO1);

  /* Swap capture buffers */
  raw_buffer_swap();
}
