#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/timer.h>
#include <libopencm3/stm32/dma.h>
#include <libopencm3/cm3/nvic.h>

#include "firmware.h"
#include "hardware.h"
#include "config.h"

#define SAMPLER_TIMER DEV_(TIM, sampler_timer)
#define SAMPLER_TIMER_RCC RCC_(TIM, sampler_timer)

#ifdef sampler_timer_ch
#define SAMPLER_TIMER_OC DEV_(TIM_OC, sampler_timer_ch)
#define SAMPLER_TIMER_EVENT _CAT3(TIM_DIER_CC, sampler_timer_ch, DE)
#define SAMPLER_TIMER_MASTER _CAT3(TIM_CR2_MMS_COMPARE_OC, sampler_timer_ch, REF)
#else
#define SAMPLER_TIMER_EVENT TIM_DIER_UDE /* Generate DMA update event */
#define SAMPLER_TIMER_MASTER TIM_CR2_MMS_UPDATE /* Generate TRGO on update */
#endif

#define SAMPLER_PERIOD ((mcu_frequency)*1000/(SND_SAMPLE_RATE))

raw_buffer_t raw_buffer_data[2];
uint8_t raw_buffer_index;

sample_t enc_buffer_data[2][SND_ENC_FRAME_SIZE];
uint8_t enc_buffer_index;

void sampler_suspend(void) {
  /* Disable ounter */
  timer_disable_counter(SAMPLER_TIMER);
}

void sampler_resume(void) {
  /* Enable counter */
  timer_enable_counter(SAMPLER_TIMER);
}

#ifdef sampler_timer_ch
static void oc_disable(enum tim_oc_id oc_id) {
  /* Disable output */
  timer_disable_oc_output(SAMPLER_TIMER, oc_id);
}

static void oc_enable(enum tim_oc_id oc_id) {
  /* Disable output */
  oc_disable(oc_id);
  
  /* Configure global mode of line */
  timer_disable_oc_clear(SAMPLER_TIMER, oc_id);
  timer_enable_oc_preload(SAMPLER_TIMER, oc_id);
  timer_set_oc_slow_mode(SAMPLER_TIMER, oc_id);
  timer_set_oc_mode(SAMPLER_TIMER, oc_id, TIM_OCM_PWM1);
  
  /* Configure OC */
  timer_set_oc_polarity_high(SAMPLER_TIMER, oc_id);
  timer_set_oc_idle_state_set(SAMPLER_TIMER, oc_id);
  
  /* Set the capture compare value for OC */
  timer_set_oc_value(SAMPLER_TIMER, oc_id, SAMPLER_PERIOD / 10);
  
  /* Enable output */
  timer_enable_oc_output(SAMPLER_TIMER, oc_id);
}
#else
#define oc_disable(...)
#define oc_enable(...)
#endif

void sampler_init(void) {
  /* Enable timer clock */
  rcc_periph_clock_enable(SAMPLER_TIMER_RCC);
  
  /* Reset TIM peripheral */
  timer_reset(SAMPLER_TIMER);
  
  /* Timer global mode:
   * - No divider
   * - Alignment edge
   * - Direction up
   */
  timer_set_mode(SAMPLER_TIMER, TIM_CR1_CKD_CK_INT,
                 TIM_CR1_CMS_EDGE, TIM_CR1_DIR_UP);
  
  /* Reset prescaler value */
  /* 72 MHz / 1000 = 72 KHz */
  timer_set_prescaler(SAMPLER_TIMER, 0);
  
  /* Enable preload */
  timer_enable_preload(SAMPLER_TIMER);
  
  /* Continuous mode */
  timer_continuous_mode(SAMPLER_TIMER);
  
  /* Period (at 8 KHz) */
  /* 72 MHz / 8 KHz - 1 = 8999 */
  /* 0...8999 = 9K ticks */
  timer_set_period(SAMPLER_TIMER, SAMPLER_PERIOD - 1);

  /* ARR reload enable */
  timer_enable_preload(SAMPLER_TIMER);
  
  /* Configure master mode */
  timer_set_master_mode(SAMPLER_TIMER, SAMPLER_TIMER_MASTER);

  /* Enable output */
  /* We need channel output to trigger ADC conversion */
  oc_enable(SAMPLER_TIMER_OC);

  /* Configure DMA update event */
  timer_set_dma_on_update_event(SAMPLER_TIMER);
  timer_update_on_overflow(SAMPLER_TIMER);
  timer_enable_update_event(SAMPLER_TIMER);

  /* Enable DMA update event */
  timer_enable_irq(SAMPLER_TIMER, SAMPLER_TIMER_EVENT);
}

void sampler_done(void) {
  /* Disable DMA update event */
  timer_disable_irq(SAMPLER_TIMER, SAMPLER_TIMER_EVENT);

  /* Disable output */
  oc_disable(SAMPLER_TIMER_OC);

  /* Disable timer clock */
  rcc_periph_clock_disable(SAMPLER_TIMER_RCC);
}
