#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/timer.h>
#include <libopencm3/cm3/nvic.h>
#include <libopencm3/stm32/exti.h>

#include <speex/speex.h>
#include <string.h>

#include "memtool.h"

#include "firmware.h"
#include "systick.h"
#include "monitor.h"

monitor_def();

#define FRAME_SIZE              160
#define ENCODED_FRAME_SIZE      20

char enc_data[ENCODED_FRAME_SIZE]; /* encoded data */
int16_t dec_buffer[2][FRAME_SIZE]; /* decoded data */

uint8_t Start_Decoding = 0;

static SpeexBits bits;/* Holds bits so they can be read and written by the Speex routines */
static void /**enc_state,*/ *dec_state;/* Holds the states of the encoder & the decoder */
static int /*quality = 4, complexity = 1, vbr = 0,*/ enh = 1;/* SPEEX PARAMETERS, MUST REMAINED UNCHANGED */

static void play(const char *const data, size_t size) {
  size_t frames;
  uint16_t sample = 0;
  uint8_t buffer;
  
  /* we prepare two buffers of decoded data: */
  for (buffer = 0; buffer < 2; buffer++) {
    memcpy(enc_data, data, ENCODED_FRAME_SIZE);
    sample += ENCODED_FRAME_SIZE;
  
    speex_bits_read_from(&bits, enc_data, ENCODED_FRAME_SIZE);
    speex_decode_int(dec_state, &bits, (spx_int16_t*)dec_buffer[buffer]);
  }

#if 0
  for (frames = 0; frames < size; ) {
    if (Start_Decoding > 0) { /* we start decoding the first buffer */
      memcpy(enc_data, data, ENCODED_FRAME_SIZE);
      sample += ENCODED_FRAME_SIZE;
      
      speex_bits_read_from(&bits, enc_data, ENCODED_FRAME_SIZE);
      speex_decode_int(dec_state, &bits, (spx_int16_t*)dec_buffer[Start_Decoding - 1]);
      
      Start_Decoding = 0;
      frames ++;
    }
  }
#endif
}

static inline void hardware_init(void) {
  /* Enable GPIO clock */
  rcc_periph_clock_enable(RCC_GPIOA);
  rcc_periph_clock_enable(RCC_GPIOB);

  gpio_set_mode(GPIOA, GPIO_MODE_OUTPUT_50_MHZ,
                GPIO_CNF_OUTPUT_PUSHPULL,
                GPIO0 | GPIO1);

  //gpio_set_mode(GPIOA, GPIO_MODE_OUTPUT_50_MHZ,
  //              GPIO_CNF_OUTPUT_ALTFN_PUSHPULL,
  //              GPIO1);
  
  /* Enable AFIO clock */
  rcc_periph_clock_enable(RCC_AFIO);
  
  /* Enable DMA1 clock */
  rcc_periph_clock_enable(RCC_DMA1);
}

static inline void hardware_done(void) {
  /* Deconfigure all GPIOs */
  gpio_set_mode(GPIOA, GPIO_MODE_INPUT,
                GPIO_CNF_INPUT_FLOAT, GPIO_ALL);
  gpio_set_mode(GPIOB, GPIO_MODE_INPUT,
                GPIO_CNF_INPUT_FLOAT, GPIO_ALL);
  
  /* Disable DMA1 clock */
  rcc_periph_clock_disable(RCC_DMA1);

  /* Disable AFIO clock */
  rcc_periph_clock_disable(RCC_AFIO);

  /* Disable GPIO clock */
  rcc_periph_clock_disable(RCC_GPIOA);
  rcc_periph_clock_disable(RCC_GPIOB);
}

static inline void init(void) {
  rcc_clock_setup_in_hse_8mhz_out_72mhz();
  
  //mem_prefill();

  hardware_init();
  capture_init();
  playback_init();
  sampler_init();
  
  systick_init();
  
  speex_bits_init(&bits);
  
  /* init speex decoder */
  dec_state = speex_decoder_init(&speex_nb_mode);
  speex_decoder_ctl(dec_state, SPEEX_SET_ENH, &enh);
}

#if 0
static inline void done(void) {
  /* deinit speex decoder */
  speex_bits_destroy(&bits);
  speex_decoder_destroy(dec_state);

  systick_done();
  
  hardware_done();
}
#else
#define done()
#endif

//#include <malloc.h>

int main(void) {
  init();

  //struct mallinfo mi = mallinfo();

  //play(example, sizeof(example));

#if 0
  for (raw_buffer_index = 0; raw_buffer_index < 2; raw_buffer_index++) {
    sample_t *s = raw_buffer_data[raw_buffer_index].sample;
    for (; s < &raw_buffer_data[raw_buffer_index].sample[SND_RAW_FRAME_SIZE]; s++) {
      *s = s - raw_buffer_data[raw_buffer_index].sample - 80;
    }
    playback_convert();
  }
#else
  memset(raw_buffer_data, 0x0, sizeof(raw_buffer_data));
#endif
  
  capture_resume();
  playback_resume();
  sampler_resume();
  //playback_sample(64);
  
  //mem_info_t info = {0, 0};
  //mem_measure(&info);
  
  monitor_init();
  
  for (;;) {
    monitor_wait();
    
    //mem_info_t info = {0, 0};
    //mem_measure(&info);
    //gpio_toggle(GPIOA, GPIO1);
  }

  //done();
  
  return 0;
}

#if 0
#include <libopencm3/cm3/scb.h>

void get_registers_from_stack(uint32_t *frameptr){
  volatile struct scb_exception_stack_frame f;

  //SCB_GET_EXCEPTION_STACK_FRAME(&f);
  memcpy(&f, frameptr, 8);
  
  /* When the following line is hit, the variables contain the register values. */
  for( ;; );
}

void hard_fault_handler(void) {
  asm volatile ("tst lr, #4\n"
                "ite eq\n"
                "mrseq r0, msp\n"
                "mrsne r0, psp\n"
                "ldr r1, [r0, #24]\n"
                "ldr r2, handler1_address_const\n"
                "bx r2\n"
                "handler1_address_const: .word get_registers_from_stack\n");
  
  while (1);
}

void mem_manage_handler(void) {
  asm volatile ("tst lr, #4\n"
                "ite eq\n"
                "mrseq r0, msp\n"
                "mrsne r0, psp\n"
                "ldr r1, [r0, #24]\n"
                "ldr r2, handler2_address_const\n"
                "bx r2\n"
                "handler2_address_const: .word get_registers_from_stack\n");
  
  while (1);
}

void bus_fault_handler(void) {
  asm volatile ("tst lr, #4\n"
                "ite eq\n"
                "mrseq r0, msp\n"
                "mrsne r0, psp\n"
                "ldr r1, [r0, #24]\n"
                "ldr r2, handler3_address_const\n"
                "bx r2\n"
                "handler3_address_const: .word get_registers_from_stack\n");
  
  while (1);
}

void usage_fault_handler(void) {
  asm volatile ("tst lr, #4\n"
                "ite eq\n"
                "mrseq r0, msp\n"
                "mrsne r0, psp\n"
                "ldr r1, [r0, #24]\n"
                "ldr r2, handler4_address_const\n"
                "bx r2\n"
                "handler4_address_const: .word get_registers_from_stack\n");
  
  while (1);
}
#endif
