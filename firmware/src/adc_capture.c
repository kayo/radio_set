#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/adc.h>
#include <libopencm3/stm32/dma.h>
#include <libopencm3/cm3/nvic.h>
//#include <libopencm3/cm3/systick.h>
//#include <libopencm3/cm3/sync.h>

#include "firmware.h"
#include "hardware.h"
#include "config.h"

#define CAPTURE_USE_INJECTED 0

#define CAPTURE_ADC DEV_(ADC, capture_adc)
#define CAPTURE_ADC_CHANNEL DEV_(ADC_CHANNEL, capture_adc_ch)
#define CAPTURE_ADC_RCC RCC_(ADC, capture_adc)

#if CAPTURE_USE_INJECTED
#define CAPTURE_DR ADC_JDR1(CAPTURE_ADC)
#define CAPTURE_EXTSEL_TIM ADC_CR2_JEXTSEL_TIM
#else
#define CAPTURE_DR ADC_DR(CAPTURE_ADC)
#define CAPTURE_EXTSEL_TIM ADC_CR2_EXTSEL_TIM
#endif

#ifdef sampler_timer_ch
#define CAPTURE_EXT_TRIGGER _CAT4(CAPTURE_EXTSEL_TIM, sampler_timer, _CC, sampler_timer_ch)
#else
#define CAPTURE_EXT_TRIGGER _CAT3(CAPTURE_EXTSEL_TIM, sampler_timer, _TRGO)
#endif

#define CAPTURE_DMA DEV_(DMA, capture_dma)
#define CAPTURE_DMA_CHANNEL DMA_CH_(capture_dma_ch)
#define CAPTURE_DMA_IRQ DMA_IRQ_(capture_dma, capture_dma_ch)
#define CAPTURE_DMA_ISR DMA_ISR_(capture_dma, capture_dma_ch)
#define CAPTURE_PORT DEV_(GPIO, capture_port)
#define CAPTURE_PADS DEV_(GPIO, capture_pad)

void capture_resume(void) {
  /* Start DMA transfer */
  dma_enable_channel(CAPTURE_DMA, CAPTURE_DMA_CHANNEL);

  /* Trigger conversion from timer */
#if CAPTURE_USE_INJECTED
  adc_enable_external_trigger_injected(CAPTURE_ADC, CAPTURE_EXT_TRIGGER);
#else
  adc_enable_external_trigger_regular(CAPTURE_ADC, CAPTURE_EXT_TRIGGER);
#endif
}

void capture_suspend(void) {
#if CAPTURE_USE_INJECTED
  adc_enable_external_trigger_injected(CAPTURE_ADC, ADC_CR2_EXTSEL_SWSTART);
#else
  adc_enable_external_trigger_regular(CAPTURE_ADC, ADC_CR2_EXTSEL_SWSTART);
#endif
  
  /* Disable the DMA channel */
  dma_disable_channel(CAPTURE_DMA, CAPTURE_DMA_CHANNEL);
}

void capture_convert(void) {
  /* pack captured unsigned samples for reading by codec */
  sample_t *w = raw_buffer_data[raw_buffer_index].sample;
  capture_sample_t *r = raw_buffer_data[raw_buffer_index].capture;
  
  for (; w < &raw_buffer_data[raw_buffer_index].sample[SND_RAW_FRAME_SIZE]; r++, w++) {
    /* round if needed */
    if (r->lsb >= SND_SAMPLE_MIDDLE) {
      r->msb ++;
    }
    /* convert to signed */
    *w = (int16_t)r->msb - SND_SAMPLE_MIDDLE;
  }
}

void capture_init(void) {
  /* Enable ADC clock */
  rcc_periph_clock_enable(CAPTURE_ADC_RCC);
  
  /* Enable DMA interrupt */
  nvic_enable_irq(CAPTURE_DMA_IRQ);
  
  /* Configure sensor GPIO */
  gpio_set_mode(CAPTURE_PORT, GPIO_MODE_INPUT,
                GPIO_CNF_INPUT_ANALOG,
                CAPTURE_PADS);
  
  /* Configure ADC subsystem */
  adc_off(CAPTURE_ADC);
  
  /* Set ADC prescaler */
  rcc_set_adcpre(8);

  adc_set_dual_mode(ADC_CR1_DUALMOD_IND);
  adc_disable_scan_mode(CAPTURE_ADC);
  adc_set_single_conversion_mode(CAPTURE_ADC);

#if CAPTURE_USE_INJECTED
  adc_disable_discontinuous_mode_regular(CAPTURE_ADC);
  adc_enable_discontinuous_mode_injected(CAPTURE_ADC);
#else
  adc_enable_discontinuous_mode_regular(CAPTURE_ADC, 1);
  adc_disable_discontinuous_mode_injected(CAPTURE_ADC);
#endif
  
  /* Left alignment mode to simply extract bytes */
  adc_set_left_aligned(CAPTURE_ADC);
  
  /* ADC frequency (ADCCLK) is 72MHz / 8 = 9MHz and period is 1/9 uS */
  /* ADC conversion time is 239.5 + 12.5 = 252 cycles */
  /* 252 cycles / 9 MHz = 28 uS */
  /* but we need 125 uS for 8 KHz, so use external trigger */
  adc_set_sample_time(CAPTURE_ADC, CAPTURE_ADC_CHANNEL, ADC_SMPR_SMP_239DOT5CYC);
  //adc_set_single_channel(CAPTURE_ADC, CAPTURE_ADC_CHANNEL);

  {
    uint8_t channels[] = { CAPTURE_ADC_CHANNEL };
    adc_set_injected_sequence(CAPTURE_ADC, sizeof(channels), channels);
  }

  adc_power_on(CAPTURE_ADC);
  adc_reset_calibration(CAPTURE_ADC);
  adc_calibration(CAPTURE_ADC);
  
  adc_enable_dma(CAPTURE_ADC);

  dma_channel_reset(CAPTURE_DMA, CAPTURE_DMA_CHANNEL);
  dma_set_priority(CAPTURE_DMA, CAPTURE_DMA_CHANNEL, DMA_CCR_PL_VERY_HIGH);
  dma_set_peripheral_size(CAPTURE_DMA, CAPTURE_DMA_CHANNEL, DMA_CCR_PSIZE_16BIT);
  dma_set_memory_size(CAPTURE_DMA, CAPTURE_DMA_CHANNEL, DMA_CCR_MSIZE_16BIT);
  dma_enable_memory_increment_mode(CAPTURE_DMA, CAPTURE_DMA_CHANNEL);
  dma_enable_circular_mode(CAPTURE_DMA, CAPTURE_DMA_CHANNEL);
  dma_set_read_from_peripheral(CAPTURE_DMA, CAPTURE_DMA_CHANNEL);
  dma_set_peripheral_address(CAPTURE_DMA, CAPTURE_DMA_CHANNEL, (uint32_t)&CAPTURE_DR);
  dma_set_memory_address(CAPTURE_DMA, CAPTURE_DMA_CHANNEL, (uint32_t)raw_buffer_data);
  dma_set_number_of_data(CAPTURE_DMA, CAPTURE_DMA_CHANNEL, sizeof(raw_buffer_data));

  /* Enable DMA interrupts */
  dma_enable_half_transfer_interrupt(CAPTURE_DMA, CAPTURE_DMA_CHANNEL);
  dma_enable_transfer_complete_interrupt(CAPTURE_DMA, CAPTURE_DMA_CHANNEL);
}

void capture_done(void) {
  /* Disable the CAPTURE_ADC */
  adc_off(CAPTURE_ADC);

  /* Disable DMA interrupts */
  dma_disable_transfer_complete_interrupt(CAPTURE_DMA, CAPTURE_DMA_CHANNEL);
  dma_disable_half_transfer_interrupt(CAPTURE_DMA, CAPTURE_DMA_CHANNEL);
  
  /* Disable DMA interrupt */
  nvic_disable_irq(CAPTURE_DMA_IRQ);
  
  /* Disable ADC clock */
  rcc_periph_clock_disable(CAPTURE_ADC_RCC);
}

void CAPTURE_DMA_ISR(void) {
  dma_clear_interrupt_flags(CAPTURE_DMA, CAPTURE_DMA_CHANNEL, DMA_GIF | DMA_TCIF | DMA_HTIF);
  
  /* Swap capture buffers */
  raw_buffer_swap();
  
  gpio_toggle(GPIOA, GPIO0);
}
