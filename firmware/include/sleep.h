#ifndef SLEEP_H
#define SLEEP_H "sleep.h"

#include <libopencm3/cm3/scb.h>

static inline void wait_for_interrupt(void) {
  asm volatile("wfi");
}

static inline void enable_sleep_on_exit(void) {
  SCB_SCR |= SCB_SCR_SLEEPONEXIT;
}

static inline void disable_sleep_on_exit(void) {
  SCB_SCR &= ~SCB_SCR_SLEEPONEXIT;
}

#endif /* SLEEP_H */
