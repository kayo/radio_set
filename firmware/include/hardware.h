#define _CAT2_(a, b) a##b
#define _CAT2(a, b) _CAT2_(a, b)

#define _CAT3_(a, b, c) a##b##c
#define _CAT3(a, b, c) _CAT3_(a, b, c)

#define _CAT4_(a, b, c, d) a##b##c##d
#define _CAT4(a, b, c, d) _CAT4_(a, b, c, d)

#define _CAT5_(a, b, c, d, e) a##b##c##d##e
#define _CAT5(a, b, c, d, e) _CAT5_(a, b, c, d, e)

#define DEV_(...) _CAT2(__VA_ARGS__)
#define RCC_(...) _CAT3(RCC_, __VA_ARGS__)

#define DMA_CH_(...) _CAT2(DMA_CHANNEL, __VA_ARGS__)
#define _DMA_IRQ_(n, c) _CAT5(NVIC_DMA, n, _CHANNEL, c, _IRQ)
#define DMA_IRQ_(...) _DMA_IRQ_(__VA_ARGS__)
#define _DMA_ISR_(n, c) _CAT5(dma, n, _channel, c, _isr)
#define DMA_ISR_(...) _DMA_ISR_(__VA_ARGS__)

#define _OR2_(t, a, b) _CAT2(t, a) | _CAT2(t, b)
#define OR2_(...) _OR2_(__VA_ARGS__)

#define _OR3_(t, a, b, c) _CAT2(t, a) | _CAT2(t, b) | _CAT2(t, c)
#define OR3_(...) _OR3_(__VA_ARGS__)
