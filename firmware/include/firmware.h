/**
 * @mainpage
 * @section design Application design
 *
 * The function switch control:
 *
 * @dot
digraph function {
  rankdir=LR;
  edge [fontsize=10];
  node [style=filled, color=mediumaquamarine, shape=box, fontsize=10];
  
  talk [label="Talk\nButton", shape=doubleoctagon];
  tx [label="Transmission"];
  rx [label="Reception"];
  
  talk -> tx [label="pressed"];
  talk -> rx [label="released"];
}
 * @enddot
 *
 * The transmission data-flow:
 *
 * @dot
digraph transmission {
  rankdir=LR;
  edge [fontsize=10];
  node [style=filled, color=lightcoral, shape=box, fontsize=10];
  
  dial [label="Dial\nButton", shape=doubleoctagon];
  adc [label="ADC\nHardware", shape=doublecircle];
  capture [label="Sound\nCapture"];
  encode [label="Speex\nEncode"];
  encrypt [label="AES\nEncrypt"];
  transmit [label="Radio\nTransmit"];
  radio [label="Radio\nHardware", shape=doublecircle];
  call [label="Call\nSignal", shape=ellipse];
  ether [label="The\nEther", shape=plaintext, color=transparent];
  
  adc -> capture -> encode -> encrypt -> transmit -> radio;
  dial -> call [label="pressed"];
  call -> encrypt;
  radio -> ether [style=dotted];
}
 * @enddot
 *
 * The reception data-flow:
 *
 * @dot
digraph reception {
  rankdir=LR;
  edge [fontsize=10];
  node [style=filled, color=lightblue, shape=box, fontsize=10];
  
  pwm [label="PWM\nHardware", shape=doublecircle];
  playback [label="Sound\nPlayback"];
  decode [label="Speex\nDecode"];
  decrypt [label="AES\nDecrypt"];
  receive [label="Radio\nReceive"];
  radio [label="Radio\nHardware", shape=doublecircle];
  bell [label="Bell\nSignal", shape=ellipse];
  ether [label="The\nEther", shape=plaintext, color=transparent];
  
  radio -> receive -> decrypt -> decode -> playback -> pwm;
  decrypt -> bell -> playback;
  ether -> radio [style=dotted];
}
 * @enddot
 *
 * @file firmware.h
 */

#ifndef FIRMWARE_H
#define FIRMWARE_H "firmware.h"

#include <stdint.h>

/**
 * @brief Max sound latency
 *
 * The maximum latency of sound in ms.
 */
#define SND_LATENCY 20

/**
 * @brief Sound sample bits
 *
 * The number of bits per each sample.
 */
#define SND_SAMPLE_BITS 8

/**
 * @brief Sound sample width
 *
 * The width of sample (for ex. 256 for 8 bits, 65536 for 16 bits).
 */
#define SND_SAMPLE_WIDTH (1 << (SND_SAMPLE_BITS))

/**
 * @brief Middle of sound sample
 *
 * The middle of sample (for ex. 128 for 8 bits, 32768 for 16 bits).
 */
#define SND_SAMPLE_MIDDLE (SND_SAMPLE_WIDTH >> 1)

/**
 * @brief Sound sample rate
 *
 * The sampling rate of sound in kHz.
 */
#define SND_SAMPLE_RATE 8

/**
 * @brief Sound frame size
 *
 * The number of samples per each captured sound frame.
 */
#define SND_RAW_FRAME_SIZE ((SND_SAMPLE_RATE) * (SND_LATENCY))

/**
 * @brief Encoding ratio
 *
 * The ratio of sound encoding.
 */
#define SND_ENC_RATIO 8

/**
 * @brief Encoded sound frame size
 *
 * The number of bytes per each encoded sound frame.
 */
#define SND_ENC_FRAME_SIZE ((SND_RAW_FRAME_SIZE) / (SND_ENC_RATIO))

/**
 * @brief Sample type
 */
#if SND_SAMPLE_BITS == 8
typedef int8_t sample_t;
#elif SND_SAMPLE_BITS == 16
typedef int16_t sample_t;
#endif

typedef union {
  uint16_t full;
  struct {
    uint8_t msb;
    uint8_t lsb;
  };
} capture_sample_t;
typedef struct {
  uint8_t positive;
  uint8_t negative;
} playback_sample_t;

/**
 * @brief Raw sound buffer for hardware accelerated I/O
 *
 * The buffers for capture and playback
 */
typedef union {
  capture_sample_t capture[SND_RAW_FRAME_SIZE];
  playback_sample_t playback[SND_RAW_FRAME_SIZE];
  sample_t sample[SND_RAW_FRAME_SIZE];
} raw_buffer_t;

/**
 * @brief Raw sound samples buffers
 *
 * The buffers for capture and playback
 */
extern raw_buffer_t raw_buffer_data[2];

/**
 * @brief Index of active buffer
 *
 * The index of buffer which currently is active
 */
extern uint8_t raw_buffer_index;

static inline void raw_buffer_swap(void) {
  raw_buffer_index = !raw_buffer_index;
}

/**
 * @brief Encoded sound data buffers
 *
 * The buffers for encode and decode
 */
extern sample_t enc_buffer_data[2][SND_ENC_FRAME_SIZE];

/**
 * @brief Index of active buffer
 *
 * The index of buffer which currently is active
 */
extern uint8_t enc_buffer_index;

/**
 * @brief Init sound sampler
 */
void sampler_init(void);

/**
 * @brief Deinit sound sampler
 */
void sampler_done(void);

void sampler_resume(void);

void sampler_suspend(void);

/**
 * @brief Init sound capture
 */
void capture_init(void);

/**
 * @brief Deinit sound capture
 */
void capture_done(void);

/**
 * @brief Resume capture
 */
void capture_resume(void);

/**
 * @brief Suspend capture
 */
void capture_suspend(void);

void capture_convert(void);

/**
 * @brief Init sound playback
 */
void playback_init(void);

/**
 * @brief Deinit sound playback
 */
void playback_done(void);

/**
 * @brief Resume playback
 */
void playback_resume(void);

/**
 * @brief Suspend playback
 */
void playback_suspend(void);

void playback_sample(sample_t sample);

void playback_convert(void);

#endif /* FIRMWARE_H */
