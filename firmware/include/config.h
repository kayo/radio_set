/**
 * @brief Main Chip frequency
 *
 * The micro-controller unit main frequency in MHz.
 */
#define mcu_frequency 72

/**
 * @brief System tick divider
 *
 * The divider for system tick.
 * Available values is 1 and 8.
 */
#define systick_divider 8

/**
 * @brief System tick interval (ms)
 *
 * The system tick interval in milliseconds.
 */
#define systick_interval 10

/**
 * @brief Sampler timer
 *
 * The timer periphery which will be used for sound sampler.
 */
#define sampler_timer 2

/**
 * @brief Sampler timer channel
 *
 * The timer channel which will be used for sound sampler.
 * You can simply undef it when channel doesn't needed.
 */
#define sampler_timer_ch 2

/**
 * @brief Capture ADC
 *
 * The ADC periphery which will be used for sound capture.
 */
#define capture_adc 1

/**
 * @brief Capture ADC channel
 *
 * The ADC channel which will be used for sound capture.
 */
#define capture_adc_ch 10

/**
 * @brief DMA for capture ADC
 *
 * The DMA periphery which will be used for driving sound capture.
 */
#define capture_dma 1

/**
 * @brief DMA channel for capture ADC
 *
 * The DMA channel which will be used for driving sound capture.
 */
#define capture_dma_ch 1

/**
 * @brief GPIO port for capture ADC input
 *
 * The name of GPIO port which will be used as input for sound capture.
 */
#define capture_port C

/**
 * @brief GPIO pad for capture ADC input
 *
 * The GPIO pad which will be used as input for sound capture.
 */
#define capture_pad 0

/**
 * @brief Playback timer
 *
 * The timer periphery which will be used for sound playback.
 * The advanced timer required because complementary outputs mast be used.
 */
#define playback_timer 1

/**
 * @brief Playback timer output X
 *
 * The output X of timer which will be used to drive bridge.
 */
#define playback_timer_ocx 1

/**
 * @brief Playback timer output Y
 *
 * The output Y of timer which will be used to drive bridge.
 */
#define playback_timer_ocy 2

/**
 * @brief DMA for playback timer
 *
 * The DMA periphery which will be used for driving sound playback.
 */
#define playback_dma 1

/**
 * @brief DMA channel for playback timer
 *
 * The DMA channel which will be used for driving sound playback.
 */
#define playback_dma_ch 7

/**
 * @brief GPIO port for playback PWM output
 *
 * The name of GPIO port which will be used as output for sound playback.
 */
#define playback_port_p A

/**
 * @brief GPIO pads for playback PWM output
 *
 * The GPIO pads which will be used as output for sound playback.
 */
#define playback_pads_p 8,9

/**
 * @brief Complementary GPIO port for playback PWM output
 *
 * The name of GPIO port which will be used as output for sound playback.
 */
#define playback_port_n B

/**
 * @brief Complementary GPIO pads for playback PWM output
 *
 * The GPIO pads which will be used as output for sound playback.
 */
#define playback_pads_n 13,14
