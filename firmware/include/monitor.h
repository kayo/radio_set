#ifndef MONITOR_H
#define MONITOR_H "monitor.h"x

#ifndef MONITOR_USE_SLEEP
#define MONITOR_USE_SLEEP 0
#endif

#ifndef MONITOR_USE_MUTEX
#define MONITOR_USE_MUTEX 0
#endif

#if MONITOR_USE_SLEEP
#include "sleep.h"
#elif MONITOR_USE_MUTEX /* && !MONITOR_USE_SLEEP */
#include <libopencm3/cm3/sync.h>
#endif

#define monitor_systick_handler(...)     \
  void sys_tick_handler(void) {          \
    /* Wake-up monitor (Main thread) */  \
    __VA_ARGS__;                         \
  }

#if MONITOR_USE_SLEEP
#define monitor_def()                              \
  monitor_systick_handler(disable_sleep_on_exit())
#elif MONITOR_USE_MUTEX /* && !MONITOR_USE_SLEEP */
#define monitor_def()                                          \
  static volatile mutex_t _monitor_tick_mutex;                 \
  monitor_systick_handler(mutex_unlock(&_monitor_tick_mutex))
#else /* !MONITOR_USE_SLEEP && !MONITOR_USE_MUTEX */
#define monitor_def()                                  \
  static volatile bool _monitor_tick_fired = false;    \
  monitor_systick_handler(_monitor_tick_fired = true)
#endif

#if MONITOR_USE_MUTEX
#define monitor_init() mutex_lock(&_monitor_tick_mutex);
#else /* !MONITOR_USE_MUTEX */
#define monitor_init()
#endif

#define monitor_done()

#if MONITOR_USE_SLEEP
#define monitor_wait()    \
  enable_sleep_on_exit(); \
  wait_for_interrupt()
#elif MONITOR_USE_MUTEX /* && !MONITOR_USE_SLEEP */
#define monitor_wait()              \
  mutex_lock(&_monitor_tick_mutex)
#else /* !MONITOR_USE_SLEEP && !MONITOR_USE_MUTEX */
#define monitor_wait() {            \
    for (; !_monitor_tick_fired; )  \
      asm volatile("nop");          \
    _monitor_tick_fired = false;    \
  }
#endif

#endif /* MONITOR_H */
